# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

setup(
    name='main',
    version='1.0.1',
    description='Wireless Router Archer C5 restart interface',
    long_description=readme,
    author='Dusan Snokhaus',
    author_email='dusan.snokhaus@snekovi.cz',
    url='https://gitlab.com/dsnokhaus/cz.snekovi.routerinterface.git',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
