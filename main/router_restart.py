import logging as log
import functools
import requests
import base64
import sys
import getopt
from datetime import date
import os
import re


def trace2log(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        log.debug(f'TRACE: calling {func.__name__}() '
                  f'with {args}, {kwargs}')

        original_result = func(*args, **kwargs)

        log.debug(f'TRACE: {func.__name__}() '
                  f'returned {original_result!r}')

        return original_result
    return wrapper


@trace2log
def restart_router(ip_address, auth_user, auth_pass):
    err = False
    url = f'http://{ip_address}/userRpm/SysRebootRpm.htm'
    http_params = {'Reboot': 'Reboot'}
    auth_str = 'Basic%20' + str(base64.b64encode(f'{auth_user}:{auth_pass}'.encode("utf-8")), "utf-8")
    http_cookies = {'Authorization': auth_str}
    http_headers = {'Referer': url}

    try:
        response = requests.get(url, params=http_params, cookies=http_cookies, headers=http_headers)

        err = (re.compile('browser does not refresh automatically').search(response.text) is None)
        log_str = f'Authorization error for user "{user_name}"'

        response.raise_for_status()
    except requests.exceptions.HTTPError as http_err:
        log_str = f'HTTP error occurred: {http_err}'
        err = True
    except Exception as exc:
        log_str = f'Other error occurred: {exc}'
        err = True
    else:
        if not err:
            log_str = 'Success'

    if err:
        log.error(log_str)
    else:
        log.info(log_str)
        print(log_str)


if __name__ == '__main__':
    ip_address = ''
    log_file = f'{date.today().strftime("%Y%m%d")}_rtres.log'
    log_dir = ''
    password = ''
    user_name = ''

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:u:p:l:", ["ip=", "user=", "pass=", "logdir="])
    except getopt.GetoptError:
        print('router-restart.py -i <ipaddress> -u <user> -p <password> -l <logdir>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('router-restart.py -i <ipaddress> -u <user> -p <password> -l <logdir>')
            sys.exit()
        elif opt in ("-i", "--ip"):
            ip_address = arg
        elif opt in ("-u", "--user"):
            user_name = arg
        elif opt in ("-p", "--pass"):
            password = arg
        elif opt in ("-l", "--logdir"):
            log_dir = arg

    if (len(ip_address.strip()) == 0 or len(user_name.strip()) == 0 or len(password.strip()) == 0):
        print('router-restart.py -i <ipaddress> -u <user> -p <password> -l <logdir>')
        sys.exit(2)

    if len(log_dir) > 0:
        log.basicConfig(filename=os.path.join(log_dir, log_file), level=log.INFO, format='%(asctime)s %(levelname)s: %(message)s')

    log.info(f'<HTTP Call {ip_address}>')
    restart_router(ip_address, user_name, password)
    log.info('</HTTP Call>')
